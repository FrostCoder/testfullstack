import React from 'react';
import './App.css';
import GmailTreeView from './components/GmailTreeView';
import request from './requests/api'
import CustomizedTreeView from './components/CustomTreeView'

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            treeLoading: true
        }
    }

    componentDidMount() {
        request.getAllDescendants(1)
            .then((res) => {
                this.setState({
                    tree: res.data.result,
                    treeLoading: false
                });
            })
            .catch((err) => {
                console.log(err)
            })
    }

    render() {
        const createTree = (data , level = 1, ancestorid = 0) => {
            return data.filter((el) => {
                return el.level === level && el.ancestorid === ancestorid
            }).map((element) => {
                element.children = createTree(data, level + 1, element.id);
                return {...element};
            }) || []
        };

        if(!this.state.treeLoading) {
            return (
                <div className="App">
                    <GmailTreeView tree={createTree(this.state.tree)}/>
                    <CustomizedTreeView tree={createTree(this.state.tree)}/>
                </div>
            )
        }
        return (<div>Loading</div>)
    }
}

export default App;