import axios from 'axios';
import config from '../config';


export default {
    getAllDescendants: (id) => {
        return axios.post(`${config.apiUrl}/getAllDescendants`,
            { id }
            )
    }
}