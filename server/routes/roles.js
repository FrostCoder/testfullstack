const express = require('express');

const router = express.Router();
const HttpStatus = require('http-status-codes');
const RoleActions = require('../app/controllers/RoleActions');


router.get('/createAdmin', async (req, res) => {
  try {
    const result = await RoleActions.createAdmin();

    if (result) {
      res.status(HttpStatus.OK).send({ id: result });
    } else {
      res.status(HttpStatus.BAD_REQUEST).send('err');
    }
  } catch (e) {
    res.status(HttpStatus.BAD_REQUEST).send(e);
  }
});

router.post('/createSubRole', async (req, res) => {
  try {
    const result = RoleActions.createSubRole(req.body.name, req.body.ancestorId);

    if (result) {
      res.status(HttpStatus.OK).send({ id: result });
    } else {
      res.status(HttpStatus.BAD_REQUEST).send('err');
    }
  } catch (e) {
    res.status(HttpStatus.BAD_REQUEST).send(e);
  }
});

router.post('/getAllDescendants', async (req, res) => {
  try {
    const result = await RoleActions.getAllDescendants(req.body.id);

    if (result) {
      res.status(HttpStatus.OK).send({ result });
    } else {
      res.status(HttpStatus.BAD_REQUEST).send('err');
    }
  } catch (e) {
    res.status(HttpStatus.BAD_REQUEST).send(e);
  }
});

router.post('/delete', async (req, res) => {
  try {
    const result = await RoleActions.deleteById(req.body.id);

    if (result) {
      res.status(HttpStatus.OK).send({ id: result });
    } else {
      res.status(HttpStatus.BAD_REQUEST).send('err');
    }
  } catch (e) {
    res.status(HttpStatus.BAD_REQUEST).send(e);
  }
});

module.exports = router;
