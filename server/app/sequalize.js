const Sequelize = require('sequelize');
const glob = require('glob');
const config = require('./config');


const sequelize = new Sequelize(
  config.DB_NAME,
  config.DB_USER,
  config.DB_PASS,
  {
    host: 'localhost',
    dialect: 'postgres',
  },
);


sequelize.sync()
  .then(() => {
    console.log('synced');
  });


module.exports = sequelize;
