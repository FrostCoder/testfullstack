const { Op } = require('sequelize');
const Role = require('../models/Role');
const sequelize = require('../sequalize');

class RoleActions {
  async createAdmin() {
    const ent = await Role.findOne({
      where: { name: 'Admin' },
    });
    if (ent) {
      return ('admin already exists');
    }
    const admin = await Role.create({
      name: 'Admin',
      view: 1,
      edit: 1,
      delete: 1,
      inherit: true,
    });
    return (admin.dataValues.id);
  }

  async createSubRole(name, ancestorId) {
    const ancestor = await Role.findOne({
      where: { id: ancestorId },
    });
    return Role.create({
      name,
      view: ancestor.dataValues.view,
      edit: ancestor.dataValues.edit,
      delete: ancestor.dataValues.delete,
      ancestorId: ancestor.dataValues.id,
      inherit: true,
    });
  }

  async UpdateByIdAndSplit(id, rights) {
    return Role.update({
      rights,
      ancestorId: 0,
    }, {
      where: {
        id,
      },
    });
  }

  async UpdateAllRoles(rights, idArray) {
    return Role.update(
      {
        ...rights,
      },
      {
        where: {
          id: {
            [Op.in]: idArray,
          },
        },
      },
    );
  }

  async getAllDescendants(id) {
    try {
      const result = await sequelize.query(
        `WITH RECURSIVE DL (id, name,view, edit, delete, ancestorId, LEVEL) AS
         (
             SELECT task.public.roles.id, task.public.roles.name,task.public.roles.view, task.public.roles.edit, 
             task.public.roles.delete, task.public.roles."ancestorId", 1 as "LEVEL"
             FROM task.public.roles
             WHERE task.public.roles.id = ${id}
             UNION ALL
             SELECT task.public.roles.id,task.public.roles.name,task.public.roles.view, task.public.roles.edit, 
             task.public.roles.delete, task.public.roles."ancestorId", PARENT.LEVEL + 1
             FROM DL as PARENT, task.public.roles
             WHERE task.public.roles."ancestorId" = PARENT.id
         )
            SELECT id, name,view, edit, delete, ancestorId, LEVEL
            FROM DL
            ORDER BY LEVEL, id`,
      );
      return result[0];
    } catch (e) {
      return 'error';
    }
  }

  async deleteById(id) {
    const deletedEnt = await Role.destroy({
      where: { id },
    });
    if (deletedEnt) {
      return `user with ${id} was deleted`;
    }
    return 'there were no user with this id';
  }
}

module.exports = new RoleActions();
