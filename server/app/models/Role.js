const Sequelize = require('sequelize');
const sequelize = require('../sequalize');

const Role = sequelize.define('role', {
  id: {
    type: Sequelize.INTEGER,
    autoIncrement: true,
    primaryKey: true,
    allowNull: false,
  },
  name: {
    type: Sequelize.STRING,
    allowNull: false,
    unique: true,
  },
  view: {
    type: Sequelize.BOOLEAN,
  },
  edit: {
    type: Sequelize.BOOLEAN,
  },
  delete: {
    type: Sequelize.BOOLEAN,
  },
  ancestorId: {
    type: Sequelize.INTEGER,
  },
  inherit: {
    type: Sequelize.BOOLEAN,
  },
}, {
  // options
});

module.exports = Role;
