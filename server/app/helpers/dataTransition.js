const getResultIdArray = (array) => resultId = array.map((ent) => ent.id);

module.exports = {
  getResultIdArray,
};
